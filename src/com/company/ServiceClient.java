package com.company;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

/**
 * Type of service client https://reqres.in/.
 * @author Alex V
 * @version 1.0
 */
public class ServiceClient {
  /**
   * Service Uri.
   */
  public String uriString = "https://reqres.in/api/users";
  
  /**
   * Method retrieves string data from the service.
   * @param userIdString the string contains the user ID
   * @return the string contains the service response
   * @throws IOException
   * @throws InterruptedException
   */
  public String getDataFromService(String userIdString) throws IOException, InterruptedException {
    var client = HttpClient.newBuilder()
      .version(HttpClient.Version.HTTP_1_1)
      .followRedirects(HttpClient.Redirect.NORMAL)
      .connectTimeout(Duration.ofSeconds(20))
      .build();
    
    var builder = HttpRequest.newBuilder();
    builder.GET();
    builder.uri(URI.create(String.format("%s/%s", uriString, userIdString)));
    
    var request = builder.build();
    var response = client.send(request, HttpResponse.BodyHandlers.ofString());
    return response.body();
  }
}