package com.company;

import com.company.SerializationClasses.Root;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;

public class Main {
  
  public static void main(String[] args) throws IOException, InterruptedException {
    
    if (args.length != 1) {
      System.out.println("Incorrect argument values was entered. Please try again.");
      return;
    }
    
    try {
      var userIdString = args[0];
      var serviceClient  = new ServiceClient();
      var jsonString = serviceClient.getDataFromService(userIdString);
      
      showUserName(jsonString);
    }
    catch (Exception ex) {
      System.out.println("An error has occurred. Contact your administrator.\n");
      System.out.println(String.format("%s\n%s", ex.getMessage(), ex.getStackTrace()));
    }
  }
  
  /**
   * Method show username from response data
   * @param responseData the string contains the service response
   * @throws IOException
   */
  public static void showUserName(String responseData) throws IOException {
    try {
      var objectMapper = new ObjectMapper();
      var root = objectMapper.readValue(responseData, Root.class);
      System.out.printf("%s %s", root.data.first_name, root.data.last_name);
    }
    catch (Exception ex) {
      System.out.println("User not found!");
    }
  }
}